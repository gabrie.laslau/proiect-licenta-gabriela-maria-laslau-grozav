import Sidebar from "./common/ChatComponents/Sidebar";
import Chat from "./common/ChatComponents/Chat";

import "../Sass/MessengerComponent.scss";
import { useContext, useState } from "react";
import { AuthContext } from "../helpers/AuthContext";

export default function MessengerComponent() {
  const { currentUser } = useContext(AuthContext);

  const [selectedConnection, setSelectedConnection] = useState();

  return (
    <div className="home">
      <div className="container">
        <Sidebar
          currentUser={currentUser}
          userSelectedCallback={setSelectedConnection}
        />
        {selectedConnection && (
          <Chat
            currentUser={currentUser}
            selectedConnection={selectedConnection}
          />
        )}
      </div>
    </div>
  );
}
