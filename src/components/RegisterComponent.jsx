import React, { useState } from 'react'
import { RegisterAPI, GoogleSignInAPI } from '../api/AuthAPI'
import { postUserData } from '../api/FirestoreAPI'
import fitclub_logo from '../assets/fitclub_logo.png'
import GoogleButton from 'react-google-button'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { getUniqueID } from '../helpers/getUniqueId'
import { firestore } from '../firebaseConfig'
import {
    doc,
    setDoc
} from 'firebase/firestore'

import '../Sass/LoginComponent.scss'

export default function RegisterComponent() {
    let navigate = useNavigate();
    const [credentials, setCredentials] = useState({});
    const register = async () => {
        try {
            let res = await RegisterAPI(credentials.email, credentials.password)
            toast.success('Cont creat cu succes.')
            postUserData({
                userID: getUniqueID(),
                name: credentials.name,
                email: credentials.email,
                imageLink: 'https://e7.pngegg.com/pngimages/985/279/png-clipart-people-running-angle-photography.png'
            })
            navigate('/home')
            localStorage.setItem('userEmail', res.user.email)
        } catch (err) {
            console.log(err);
            toast.error('Nu se poate crea acest cont.')
        }
    };

    const googleSignIn = () => {
        let response = GoogleSignInAPI()
        navigate('/home')
    }
    return (
        <div className='login-wrapper'>
            <div className='login-wrapper-inner'>
                <img src={fitclub_logo} className='fitclubLogo' />
                <h1 className='heading'> Rămâi conectat și activ </h1>
                <p className='sub-heading'>Înregistrare</p>

                <div className='auth-inputs'>
                    <input
                        onChange={(event) =>
                            setCredentials({ ...credentials, name: event.target.value })
                        }
                        type='text'
                        className='common-input'
                        placeholder='Numele și prenumele'
                    />
                    <input
                        onChange={(event) =>
                            setCredentials({ ...credentials, email: event.target.value })
                        }
                        type='email'
                        className='common-input'
                        placeholder='Introduceți un email'
                    />
                    <input
                        onChange={(event) =>
                            setCredentials({ ...credentials, password: event.target.value })
                        }
                        type='password'
                        className='common-input'
                        placeholder='Introduceți o parolă (minim 6 caractere)'
                    />
                </div>
                <button onClick={register} className='login-btn'>
                    Înregistrare
                </button>
            </div>
            <hr className='hr-text'/>
            <div className='google-btn-container'>
                <GoogleButton
                    className='google-btn'
                    onClick={googleSignIn}
                />

                <p className='go-to-signup'>
                    Aveți deja un cont în FitClub?{" "}
                    <span className='join-now' onClick={() => navigate('/')}>
                        Autentificare
                    </span>
                </p>
            </div>
        </div>
    )
}


