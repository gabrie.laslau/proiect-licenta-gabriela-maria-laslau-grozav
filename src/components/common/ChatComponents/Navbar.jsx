import React from 'react'
import '../../../Sass/MessengerComponent.scss'
import { useContext } from 'react'
import { AuthContext } from '../../../helpers/AuthContext'

export default function Navbar() {
  const { currentUser } = useContext(AuthContext)

  return (
    <div className='navbar'>
      <span className='logo'>Utilizatorii cu care s-a realizat conexiunea</span>
      <div className="user">
        <span>{currentUser.displayName}</span>
      </div>
    </div>
  )
}
