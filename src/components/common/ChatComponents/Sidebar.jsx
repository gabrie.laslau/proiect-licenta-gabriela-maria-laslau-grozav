import { useState, useMemo } from "react";
import "../../../Sass/MessengerComponent.scss";
import Navbar from "./Navbar";
import { getMyConnections } from "../../../api/FirestoreAPI";

export default function Sidebar({ currentUser, userSelectedCallback }) {
  const [connections, setConnections] = useState([]);

  useMemo(() => {
    getMyConnections(currentUser.id, setConnections);
  }, [currentUser]);

  function getOtherConnectionMemberId(connection) {
    if (connection.targetId === currentUser.id) {
      return connection.userId;
    }

    return connection.targetId;
  }

  console.log(connections);
  console.log(currentUser);

  return (
    <div className="sidebar">
      <Navbar />
      {connections.map((connection) => (
        <div
          className="chatUserRow"
          key={connection.targetId + connection.userId}
          onClick={() => userSelectedCallback(connection)}
        >
          <p>{connection[getOtherConnectionMemberId(connection)]}</p>
        </div>
      ))}
    </div>
  );
}
