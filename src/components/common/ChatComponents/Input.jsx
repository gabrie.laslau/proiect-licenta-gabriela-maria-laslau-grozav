import { useMemo, useState } from "react";
import "../../../Sass/MessengerComponent.scss";
import "../LikeButton/index.scss";
import Img from "../../../assets/img.png";
import Attach from "../../../assets/attach.png";
import { getChatMessages, postChatMessage } from "../../../api/FirestoreAPI";
import { getCurrentTimeStamp } from "../../../helpers/useMoment";

export default function Input({ currentUser, connection }) {
  const [message, setMessage] = useState("");
  const [messages, setChatMessages] = useState([]);

  const getMessage = (event) => {
    setMessage(event.target.value);
  };

  const addMessage = () => {
    postChatMessage(
      message,
      getCurrentTimeStamp("LLL"),
      currentUser,
      connection
    );
    setMessage("");
  };

  useMemo(() => {
    getChatMessages(connection, setChatMessages);
  }, [connection]);

  return (
    <div className="chatContent">
      <div className="input">
        <input
          type="text"
          placeholder="Scrie un mesaj .."
          onChange={getMessage}
          className="comment-input"
          name="message"
          value={message}
        />
        <div className="send">
          <img src={Attach} alt="" />
          <input type="file" style={{ display: "none" }} id="file" />
          <label htmlFor="file">
            <img src={Img} alt="" />
          </label>
          <button onClick={addMessage}>Trimite</button>
        </div>
      </div>
      <div className="all-messages">
        {messages.length > 0 ? (
          messages.map((message, index) => {
            return (
              <div className="one-comments" key={message.timeStamp + index}>
                <p className="name">{message.name}</p>
                <p className="timestamp">{message.timeStamp}</p>
                <p className="message">{message.message}</p>
              </div>
            );
          })
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}
