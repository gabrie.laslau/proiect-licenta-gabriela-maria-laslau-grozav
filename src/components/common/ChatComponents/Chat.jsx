import Input from "./Input";
import fitclub_logo from "../../../assets/fitclub_logo.png";
import "../../../Sass/MessengerComponent.scss";

export default function Chat({ currentUser, selectedConnection }) {
  function getOtherConnectionMemberId() {
    if (selectedConnection.targetId === currentUser.id) {
      return selectedConnection.userId;
    }

    return selectedConnection.targetId;
  }

  return (
    <div className="chat">
      <div className="chatInfo">
        <span className="span-info">{selectedConnection[getOtherConnectionMemberId()]}</span>
        <div className="chatIcons">
          <img src={fitclub_logo} alt="" />
        </div>
      </div>
      <Input currentUser={currentUser} connection={selectedConnection} />
    </div>
  );
}
