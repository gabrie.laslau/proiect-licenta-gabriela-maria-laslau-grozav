import { useRef, useState, useMemo } from "react";
import { getMessages, postMessage } from "../../../api/FirestoreAPI";
import Img from "../../../assets/img.png";
import Attach from "../../../assets/attach.png";
import { getCurrentTimeStamp } from "../../../helpers/useMoment";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import "./index.scss";

const ChatBox = ({ currentUser }) => {
  let navigate = useNavigate();

  // const goToRoute = (route) => {
  //   navigate(route);
  // };
  // const scroll = useRef();
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);

  const getMessage = (event) => {
    setMessage(event.target.value);
  };

  const addMessage = async (event) => {
    event.preventDefault();
    if (message.trim() === "") {
      toast.error("Introduceți un mesaj ..");
      return;
    }
    postMessage(message, getCurrentTimeStamp("LLL"), currentUser?.name);
    setMessage("");
  };

  const openUser = (user) => {
    navigate("/profile", {
      state: {
        id: user.id,
        email: user.email,
      },
    });
  };

  useMemo(() => {
    getMessages(setMessages);
  }, []);

  return (
    <div className="chatbox">
      <div className="input-data">
        <div className="input-row">
          <input
            type="text"
            placeholder="Scrie un mesaj .."
            onChange={getMessage}
            className="comment-input"
            name="message"
            value={message}
          />
          <div className="send">
            <label htmlFor="file">
              <img className="img-row" src={Img} alt="" />
            </label>
            <button className="button-type" onClick={addMessage}>
              Trimite
            </button>
          </div>
        </div>

        {messages.length > 0 ? (
          messages.map((message) => {
            return (
              <div className={`chat-bubble ${currentUser ? "right" : ""}`}>
                <img
                  className="chat-bubble__left"
                  src={currentUser.imageLink}
                  alt="user avatar"
                />
                <div className="chat-bubble__right">
                  <p className="name">{message.name}</p>
                  <p className="timestamp">{message.timeStamp}</p>
                  <p className="message">{message.message}</p>
                </div>
              </div>
            );
          })
        ) : (
          <></>
        )}
      </div>
    </div>
  );
};

export default ChatBox;

/**   <main className="chat-box">
            <div className="messages-wrapper">
                {messages?.map((message) => (
                    <Message key={message.id} message={message} />
                ))}
            </div>
            { }
            <span ref={scroll}></span>
            <SendMessage scroll={scroll} />
        </main> */
