import React, { useState, useEffect } from 'react'
import {
  AiOutlineHome,
  AiOutlineUserSwitch,
  AiOutlineSearch,
  AiOutlineMessage,
  AiOutlineWechat
} from 'react-icons/ai'

import { IoIosFitness } from 'react-icons/all'
import { useNavigate } from 'react-router-dom'
import { getAllUsers } from '../../../api/FirestoreAPI'
import ProfilePopup from '../ProfilePopup'
import Search from '../Search'
import fitclub_logo from '../../../assets/fitclub_logo.png'

import './index.scss'

export default function Topbar({ currentUser }) {

  let navigate = useNavigate()

  const [popupVisible, setPopupVisible] = useState(false)
  const [isSearch, setIsSearch] = useState(false)
  const [users, setUsers] = useState([])
  const [filteredUsers, setFilteredUsers] = useState([])
  const [searchInput, setSearchInput] = useState('')

  const goToRoute = (route) => {
    navigate(route)
  }

  const displayPopup = () => {
    setPopupVisible(!popupVisible)
  }

  const openUser = (user) => {
    navigate('/profile', {
      state: {
        id: user.id,
        email: user.email,
      },
    })
  }

  const handleSearch = () => {
    if (searchInput !== '') {
      let searched = users.filter((user) => {
        return Object.values(user)
          .join('')
          .toLowerCase()
          .includes(searchInput.toLowerCase())
      });

      setFilteredUsers(searched)
    } else {
      setFilteredUsers(users)
    }
  }

  useEffect(() => {
    let debounced = setTimeout(() => {
      handleSearch()
    }, 1000)

    return () => clearTimeout(debounced)
  }, [searchInput])

  useEffect(() => {
    getAllUsers(setUsers)
  }, [])

  return (
    <div className='topbar-main'>
      {popupVisible ? (
        <div className='popup-position'>
          <ProfilePopup />
        </div>
      ) : (
        <></>
      )}

      <img
        className='fitclub-logo'
        src={fitclub_logo}
        alt='fitclub_logo'
      />
      {isSearch ? (
        <Search
          setIsSearch={setIsSearch}
          setSearchInput={setSearchInput}
        />
      ) : (
        <div className='react-icons'>
          <AiOutlineSearch
            size={25}
            className='react-icon'
            onClick={() => setIsSearch(true)}
          />
          <label>
            Căutare
          </label>

          <AiOutlineHome
            size={25}
            className='react-icon'
            onClick={() => goToRoute('/home')}
          />
          <label>
            Acasă
          </label>

          <AiOutlineUserSwitch
            size={25}
            className='react-icon'
            onClick={() => goToRoute('/connections')}
          />
            <label>
              Conecțiuni
            </label>
          <IoIosFitness
            size={25}
            className='react-icon'
            onClick={() => goToRoute('/fitness')}
          />
         
         <label>
              Fitness
            </label>
          <AiOutlineMessage
            size={25}
            className='react-icon'
            onClick={() => goToRoute('/chat')}
          />
            <label>
             Grup
            </label>

          <AiOutlineWechat
            size={25}
            className='react-icon'
            onClick={() => goToRoute('/messenger')}
          />
          <label>
             Mesagerie
            </label>
        </div>
      )}

      <img
        className='user-logo'
        src={currentUser.imageLink}
        alt='user-logo'
        onClick={displayPopup}
      />

      {searchInput.length === 0 ? (
        <></>
      ) : (
        <div className='search-results'>
          {filteredUsers.length === 0 ? (
            <div className='search-inner'>Niciun rezultat găsit</div>
          ) : (
            filteredUsers.map((user) => (
              <div
                className='search-inner'
                onClick={() => openUser(user)}
              >
                <img src={user.imageLink} />
                <p className='name'>{user.name}</p>
              </div>
            ))
          )}
        </div>
      )}
    </div>
  )
}