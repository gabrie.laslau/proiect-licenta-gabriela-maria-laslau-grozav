import { useMemo, useState } from "react";
import "./index.scss";
import { AiOutlineLike, AiFillLike } from "react-icons/ai";
import { MdModeComment } from "react-icons/md";
import {
  likePost,
  getLikesByUser,
  postComment,
  getComments,
} from "../../../api/FirestoreAPI";
import { getCurrentTimeStamp } from "../../../helpers/useMoment";

export default function LikeButton({ userId, postId, currentUser }) {
  const [likesCount, setLikesCount] = useState(0);
  const [showCommentBox, setShowCommentBox] = useState(false);
  const [liked, setLiked] = useState(false);
  const [comment, setComment] = useState("");
  const [comments, setComments] = useState([]);

  const handleLike = () => {
    likePost(userId, postId, liked);
  };

  const getComment = (event) => {
    setComment(event.target.value);
  };

  const addComment = () => {
    postComment(postId, comment, getCurrentTimeStamp("LLL"), currentUser?.name);
    setComment("");
  };

  useMemo(() => {
    getLikesByUser(userId, postId, setLiked, setLikesCount);
    getComments(postId, setComments);
  }, [userId, postId]);

  return (
    <div className="like-container">
      <p> {likesCount} persoane au apreciat această postare.</p>
      <div className="hr-line">
        <hr />
      </div>
      <div className="like-comment">
        <div className="like-comment-inner" onClick={handleLike}>
          {liked ? (
            <AiFillLike size={25} color="#0a66c2" />
          ) : (
            <AiOutlineLike size={25} />
          )}
          <p className={liked ? "blue" : "black"}>Apreciere</p>
        </div>
        <div
          className="like-comment-inner"
          onClick={() => setShowCommentBox(!showCommentBox)}
        >
          {
            <MdModeComment
              size={25}
              color={showCommentBox ? "#0a66c2" : "black"}
            />
          }

          <p className={showCommentBox ? "blue" : "black"}>Comentarii</p>
        </div>
      </div>
      {showCommentBox ? (
        <>
          <input
            onChange={getComment}
            placeholder="Adăugați un comentariu"
            className="comment-input"
            name="comment"
            value={comment}
          />
          <button className="add-comment-btn" onClick={addComment}>
            Adaugă
          </button>

          {comments.length > 0 ? (
            comments.map((comment) => {
              return (
                <div className="all-comments" key={comment.id}>
                  <p className="name">{comment.name}</p>
                  <p className="comment">{comment.comment}</p>
                  <p className="timestamp">{comment.timeStamp}</p>
                </div>
              );
            })
          ) : (
            <></>
          )}
        </>
      ) : (
        <></>
      )}
    </div>
  );
}
