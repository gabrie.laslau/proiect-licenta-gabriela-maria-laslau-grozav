import React, { useState } from 'react'
import { editProfile } from '../../../api/FirestoreAPI'
import { AiOutlineClose } from 'react-icons/ai'
import './index.scss'

export default function ProfileEdit({ onEdit, currentUser }) {
    const [editInputs, setEditInputs] = useState(currentUser);

    const getInput = (event) => {
        let { name, value } = event.target;
        let input = { [name]: value };
        setEditInputs({ ...editInputs, ...input });
    };

    const updateProfileData = async () => {
        await editProfile(currentUser?.id, editInputs);
        await onEdit();
    };

    const options = [
        { value: 'trainer', label: 'Trainer' },
        { value: 'fitnessEnthusiast', label: 'Fitness enthusiast' },
        { value: 'recreationalAthlete', label: 'Recreational athlete' },
    ];

    return (
        <div className='profile-card'>
            <div className='edit-btn'>
                <AiOutlineClose className='close-icon' onClick={onEdit} size={20} />
            </div>

            <div className='profile-edit-inputs'>
                <label>Numele complet</label>
                <input
                    onChange={getInput}
                    className='common-input'
                    placeholder='Nume-prenume'
                    name='name'
                    value={editInputs.name}
                />

                <label>O scurtă descriere a statutului</label>
                <input
                    onChange={getInput}
                    className='common-input'
                    placeholder='ex. Antrenor | Înepător  | Atlet'
                    name='headline'
                    value={editInputs.headline}
                />
                <label>Țară</label>
                <input
                    onChange={getInput}
                    className='common-input'
                    placeholder='Țară/Regiune'
                    name='country'
                    value={editInputs.country}
                />
                <label>Oraș</label>
                <input
                    onChange={getInput}
                    className='common-input'
                    placeholder='Oraș'
                    name='city'
                    value={editInputs.city}
                />
                <label>Ocupație actuală</label>
                <input
                    onChange={getInput}
                    className='common-input'
                    placeholder='Ocupație'
                    name='occupation'
                    value={editInputs.occupation}
                />
                <label>Educație</label>
                <input
                    onChange={getInput}
                    className='common-input'
                    placeholder='Educație'
                    name='studies'
                    value={editInputs.studies}
                />
                <label>Hobbie-uri</label>
                <input
                    onChange={getInput}
                    className='common-input'
                    placeholder='Hobbies'
                    name='hobbies'
                    value={editInputs.hobbies}
                />
                <label>Website</label>
                <input
                    onChange={getInput}
                    className='common-input'
                    placeholder='Website'
                    name='website'
                    value={editInputs.website}
                />
                <label>Despre mine</label>
                <textarea
                    className='common-textArea'
                    placeholder='Despre mine'
                    onChange={getInput}
                    rows={5}
                    name='aboutMe'
                    value={editInputs.aboutMe}
                />
                <label>Cursuri & Training-uri</label>
                <input
                    onChange={getInput}
                    className='common-input'
                    placeholder='Ce mă recomandă?'
                    name='training'
                    value={editInputs.training}
                />
            </div>
            <div className='save-container'>
                <button
                    className='save-btn'
                    onClick={updateProfileData}>
                    Salvare
                </button>
            </div>
        </div>
    )
}
