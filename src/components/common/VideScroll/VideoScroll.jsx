import React from 'react';
import './index.scss'; // CSS file for styling

export default function VideoScroll({ videos }) {
    return (
        <div className="video-scroll">
            <div className="video-scroll-content">
                {videos.map((video, index) => (
                    <div className="video-card" key={index}>
                        <video src={video.id} controls />
                    </div>
                ))}
            </div>
        </div>
    );
}