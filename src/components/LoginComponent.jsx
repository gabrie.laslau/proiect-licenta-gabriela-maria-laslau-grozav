import React, { useState } from "react";
import { LoginAPI } from "../api/AuthAPI";
import fitclub_logo from "../assets/fitclub_logo.png";
import { useNavigate } from "react-router-dom";
import "../Sass/LoginComponent.scss";
import { toast } from "react-toastify";

export default function LoginComponent() {
    let navigate = useNavigate();
    const [credentails, setCredentials] = useState({});

    // funcție pentru conectare
    const login = async () => {
        try {
            let res = await LoginAPI(credentails.email, credentails.password);
            toast.success("Autentificare reusită în FitClub!");
            localStorage.setItem("userEmail", res.user.email);
            navigate("/home");
        } catch (err) {
            console.log(err);
            toast.error("Te rog, verifică datele de autentificare!");
        }
    };

    return (
        <div className="login-wrapper">
            <div className="login-wrapper-inner">
                <img className="fitclubLogo" src={fitclub_logo} />
                <h1 className="heading">Autentificare</h1>
                <p className="sub-heading">Rămâi în trend cu ultimele noutăți legate de un stil de viată activ</p>

                <div className="auth-inputs">
                    <input
                        onChange={(event) =>
                            setCredentials({ ...credentails, email: event.target.value })
                        }
                        type="email"
                        className="common-input"
                        placeholder="Adresă de email"
                    />
                    <input
                        onChange={(event) =>
                            setCredentials({ ...credentails, password: event.target.value })
                        }
                        type="password"
                        className="common-input"
                        placeholder="Parolă"
                    />
                </div>
                <button onClick={login} className="login-btn">
                    Autentificare
                </button>
            </div>
            <hr className="hr-text" />
            <div className="google-btn-container">
                <p className="go-to-signin">
                    Nou în FitClub?{" "}
                    <span className="join-now" onClick={() => navigate("/register")}>
                        Alăturați-vă
                    </span>
                </p>
            </div>
        </div>
    );
}