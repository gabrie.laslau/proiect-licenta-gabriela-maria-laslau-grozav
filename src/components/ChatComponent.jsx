import ChatBox from "./common/Chat/ChatBox";
import "../Sass/ChatComponent.scss";

export default function ChatComponent({ currentUser }) {
  return (
    <div className="App">
      <ChatBox currentUser={currentUser} />
    </div>
  );
}
