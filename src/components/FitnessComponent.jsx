import React from 'react'
import { Container } from 'react-bootstrap'
import fitclub_logo from '../assets/fitclub_logo.png'
import {
    Card,
    CardHeader,
    CardBody,
    Typography,
    Button,
} from '@material-tailwind/react'

import '../Sass/FitnessComponent.scss'

export default function FitnessComponent() {

    return (
        <div className='container'>
            <img
                className='fit-image'
                src={fitclub_logo}
                alt='fit-image'
            />

            { /* <div className='card-container'>
                <Card className="card-class">
                    <CardHeader
                        shadow={false}
                        floated={false}
                        className="w-2/5 shrink-0 m-0 rounded-r-none"
                    >
                        <img
                            src="https://www.stiridirecte.ro/wp-content/uploads/2020/08/cat-castiga-un-antrenor-personal-scaled.jpg"
                            alt="image"
                            className='card-image'
                        />
                    </CardHeader>
                    <CardBody>
                        <Typography className='topography-card'>Start Up</Typography>
                        <Typography className='topography-card'>
                            If is the first time for you here and wonder that is the next step ..
                        </Typography>
                        <Typography className='topography-card'>
                            This is the first step you need to explore since you choose to join in FitClub.
                        </Typography>
                        <a href="#" className="inline-block">
                            <Button variant="text" className="button-card">
                                Learn More
                            </Button>
                        </a>
                    </CardBody>
                </Card>
            </div>*/}

            <h2 className='h4-text'>ANTRENAMENTE</h2>

            <p className='p-text'>
                Activitatea fizică vă poate întări oasele și mușchii, vă poate ajuta să vă mențineți o greutate sănătoasă, să vă creșteți capacitatea de a îndeplini sarcinile zilnice și să vă îmbunătățiți sănătatea cognitivă. Adulții care petrec mai puțin timp stând și se angajează în orice nivel de exerciții fizice moderate până la viguroase culeg unele beneficii pentru sănătate.            </p>
            <p className='p-text'>
                Exercițiile fizice sunt cruciale pentru gestionarea greutății și prevenirea obezității, pe lângă alimente. Aportul zilnic de calorii și cheltuielile de energie trebuie să se echilibreze pentru a vă menține greutatea. Trebuie să arzi mai multe calorii decât consumi pentru a pierde în greutate.
                Inima și circulația sunt îmbunătățite prin exerciții fizice. Fluxul sanguin crescut crește nivelul de oxigen al corpului. Ca urmare, șansa dumneavoastră de a dezvolta probleme cardiace, cum ar fi colesterolul excesiv, boala coronariană și atacurile de cord este redusă. În plus, exercițiile fizice regulate ajută la scăderea nivelului lipidelor și al tensiunii arteriale.
            </p>

            <Container>
                <div className='video-container'>
                    <iframe
                        width="560"
                        height="315"
                        src="https://www.youtube.com/embed/-YpRYNREDV8?controls=0"
                        allowFullScreen
                    >
                    </iframe>
                </div>
                <p className='p-text'>Puteți găsi mai multe videoclipuri asemănătoare mai jos.</p>
            </Container>
            <h3 className='h3-text'>HIIT</h3>
            <p className='p-text'>
                Ești gata să-ți faci inima să bată repede? Poți arde acele calorii cu ajutorul acestor exerciții aerobe și HIIT!
            </p>
            <Container className='container-row' >
                <div className='hiit-container'>
                    <iframe
                        width="430"
                        height="235"
                        src="https://www.youtube.com/embed/mBO_61r7CIY?controls=0"
                        allowFullScreen
                    >
                    </iframe>
                </div>
                <div className='hiit-container'>
                    <iframe
                        width="430"
                        height="235"
                        src="https://www.youtube.com/embed/eheChQLYJSg?controls=0"
                        allowFullScreen
                    >
                    </iframe>
                </div>
                <div className='hiit-container'>
                    <iframe
                        width="430"
                        height="235"
                        src="https://www.youtube.com/embed/JDgc6CxwEMI?controls=0"
                        allowFullScreen
                    >
                    </iframe>

                </div>
            </Container>


            <h3 className='h3-text'>Abs</h3>
            <p className='p-text'>
                Exercițiile pentru abdomene vizează în mod specific mușchii din nucleul tău, inclusiv rectul abdominal, oblicii și abdomenul transversal. Întărirea acestor mușchi ajută la îmbunătățirea stabilității nucleului, care este esențială pentru menținerea unei posturii adecvate, a echilibrului și a forței generale a corpului.

            </p>
            <p className='p-text'>
                Un nucleu puternic este vital pentru efectuarea diferitelor activități atletice. Indiferent dacă alergați, săriți, ridicați greutăți sau participați la sport, un nucleu solid oferă o bază stabilă de sprijin, îmbunătățește transferul de putere între partea superioară și cea inferioară a corpului și ajută la prevenirea rănilor.
                Mușchii abdominali slabi pot contribui la o postură proastă, ducând la dureri de spate și disconfort. Întărindu-vă abdomenul, vă puteți îmbunătăți postura și alinierea, reducând tensiunea asupra coloanei vertebrale și promovând o poziție mai dreaptă și mai echilibrată.
            </p>
            <Container className='container-row'>
                <div className='video-container'>
                    <iframe
                        width="450"
                        height="235"
                        src="https://www.youtube.com/embed/U3767euCJI4?controls=0"
                        allowFullScreen
                    >
                    </iframe>
                </div>
                <div className='video-container'>
                    <iframe
                        width="450"
                        height="235"
                        src="https://www.youtube.com/embed/AnYl6Nk9GOA?controls=0"
                        allowFullScreen
                    >
                    </iframe>
                </div>
                <div className='video-container'>
                    <iframe
                        width="450"
                        height="235"
                        src="https://www.youtube.com/embed/kFqVB3v9Jk0"
                        allowFullScreen
                    >
                    </iframe>
                </div>
            </Container>
            <h3 className='h3-text'>10 Minutes</h3>
            <p className='p-text'>
                Angajarea în 10 minute de exercițiu pe zi poate oferi mai multe beneficii pentru sănătatea și bunăstarea dumneavoastră generală.
                Deși poate părea o perioadă scurtă de timp, exercițiile zilnice consecvente, chiar și în explozii mai scurte, pot avea un impact pozitiv.
            </p>
            <p className='p-text'>
                Exercițiile fizice regulate, chiar și pe durate scurte, pot ajuta la întărirea inimii și la îmbunătățirea circulației sângelui. Poate reduce riscul de a dezvolta boli cardiovasculare, cum ar fi boli de inimă și hipertensiune arterială.
                Exercițiile fizice stimulează producția de endorfine, care sunt hormoni naturali care stimulează starea de spirit. Doar 10 minute de exerciții fizice vă pot ajuta să vă creșteți nivelul de energie, făcându-vă să vă simțiți mai alert și mai concentrat pe parcursul zilei.
            </p>
            <Container>
                <div className='video-container'>
                    <iframe
                        width="550"
                        height="335"
                        src="https://www.youtube.com/embed/OjQBKsfzdZs?controls=0"
                        allowFullScreen
                    >
                    </iframe>
                </div>
            </Container>
            <h3 className='h3-text'>20+ Minutes</h3>
            <p className='p-text'>
                În timp ce angajarea în 10 minute de exerciții pe zi poate oferi beneficii, creșterea duratei la mai mult de 20 de minute pe zi oferă avantaje suplimentare pentru sănătatea și fitness-ul dumneavoastră.
                Cu cât faci mai mult exerciții, cu atât vei arde mai multe calorii. Acest lucru poate fi deosebit de important pentru managementul greutății și obiectivele de pierdere în greutate. Exercitarea mai mult de 20 de minute vă permite să consumați o cantitate mai mare de energie, ajutându-vă să creați un deficit de calorii și să vă atingeți potențial greutatea dorită.
            </p>
            <Container className='container-row'>
                <div className='video-container'>
                    <iframe
                        width="550"
                        height="335"
                        src="https://www.youtube.com/embed/KaO4k_syMnQ?controls=0"
                        allowFullScreen
                    >
                    </iframe>
                </div>
                <div className='video-container'>
                    <iframe
                        width="550"
                        height="335"
                        src="https://www.youtube.com/embed/ljNgkSctkXg?controls=0"
                        allowFullScreen
                    >
                    </iframe>
                </div>
            </Container>
        </div>
    )
}
