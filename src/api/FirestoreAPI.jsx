import { firestore } from "../firebaseConfig";
import {
  addDoc,
  collection,
  onSnapshot,
  doc,
  updateDoc,
  query,
  where,
  setDoc,
  getDocs,
  deleteDoc,
  orderBy,
  arrayUnion,
  or,
} from "firebase/firestore";
import { toast } from "react-toastify";
import { getUniqueID } from "../helpers/getUniqueId";

let postsRef = collection(firestore, "posts");
let userRef = collection(firestore, "users");
let likeRef = collection(firestore, "likes");
let commentsRef = collection(firestore, "comments");
let connectionRef = collection(firestore, "connections");
let chatUsersRef = collection(firestore, "userChats");
let messageRef = collection(firestore, "messages");
const chatRoomsRef = collection(firestore, "chatRooms");

export const postStatus = (object) => {
  addDoc(postsRef, object)
    .then(() => {
      toast.success("Postare adăugată cu succes.");
    })
    .catch((err) => {
      console.log(err);
    });
};

export const searchUser = (searchUser, username) => {
  try {
    const q = query(userRef, where("name", "==", username));
    onSnapshot(q, (response) => {
      searchUser(
        response.docs.map((docs) => {
          return { ...docs.data(), id: docs.id };
        })[0]
      );
    });
  } catch (err) {
    // FIXME: no setErr function defined
    setErr(true);
  }
};

export const getStatus = (setAllStatus) => {
  onSnapshot(postsRef, (response) => {
    setAllStatus(
      response.docs.map((docs) => {
        return { ...docs.data(), id: docs.id };
      })
    );
  });
};

export const getAllUsers = (setAllUsers) => {
  onSnapshot(userRef, (response) => {
    setAllUsers(
      response.docs.map((docs) => {
        return { ...docs.data(), id: docs.id };
      })
    );
  });
};

export const getSingleStatus = (setAllStatus, id) => {
  const singlePostQuery = query(postsRef, where("userID", "==", id));
  onSnapshot(singlePostQuery, (response) => {
    setAllStatus(
      response.docs.map((docs) => {
        return { ...docs.data(), id: docs.id };
      })
    );
  });
};

export const getSingleUser = (setCurrentUser, email) => {
  const singleUserQuery = query(userRef, where("email", "==", email));
  onSnapshot(singleUserQuery, (response) => {
    setCurrentUser(
      response.docs.map((docs) => {
        return { ...docs.data(), id: docs.id };
      })[0]
    );
  });
};

export const postUserData = (object) => {
  addDoc(userRef, object)
    .then(() => {})
    .catch((err) => {
      console.log(err);
    });
};

export const getCurrentUser = (setCurrentUser) => {
  onSnapshot(userRef, (response) => {
    console.log("<FirestoreAPI> current user", response.doc);

    setCurrentUser(
      response.docs
        .map((docs) => {
          return { ...docs.data(), id: docs.id };
        })
        .filter((item) => {
          return item.email === localStorage.getItem("userEmail");
        })[0]
    );
  });
};

export const editProfile = (userID, payload) => {
  let userToEdit = doc(userRef, userID);

  updateDoc(userToEdit, payload)
    .then(() => {
      toast.success("Profilul a fost actualizat cu succes.");
    })
    .catch((err) => {
      console.log(err);
    });
};

export const likePost = (userId, postId, liked) => {
  try {
    let docToLike = doc(likeRef, `${userId}_${postId}`);
    if (liked) {
      deleteDoc(docToLike);
    } else {
      setDoc(docToLike, { userId, postId });
    }
  } catch (err) {
    console.log(err);
  }
};

export const getLikesByUser = (userId, postId, setLiked, setLikesCount) => {
  try {
    let likeQuery = query(likeRef, where("postId", "==", postId));

    onSnapshot(likeQuery, (response) => {
      let likes = response.docs.map((doc) => doc.data());
      let likesCount = likes?.length;

      const isLiked = likes.some((like) => like.userId === userId);

      setLikesCount(likesCount);
      setLiked(isLiked);
    });
  } catch (err) {
    console.log(err);
  }
};

export const postComment = (postId, comment, timeStamp, name) => {
  try {
    addDoc(commentsRef, {
      postId,
      comment,
      timeStamp,
      name,
    });
  } catch (err) {
    console.log(err);
  }
};

export const postMessage = (message, timeStamp, name) => {
  try {
    addDoc(messageRef, {
      message,
      timeStamp,
      name,
    });
  } catch (err) {
    console.log(message);
    console.log(err);
  }
};

export const getMessages = (setMessages) => {
  try {
    let singlePostQuery = query(messageRef, orderBy("timeStamp", "desc"));

    onSnapshot(singlePostQuery, (response) => {
      const messages = response.docs.map((doc) => {
        return {
          id: doc.id,
          ...doc.data(),
        };
      });

      setMessages(messages);
    });
  } catch (err) {
    console.log(err);
  }
};

export const getComments = (postId, setComments) => {
  try {
    let singlePostQuery = query(commentsRef, where("postId", "==", postId));

    onSnapshot(singlePostQuery, (response) => {
      const comments = response.docs.map((doc) => {
        return {
          id: doc.id,
          ...doc.data(),
        };
      });

      setComments(comments);
    });
  } catch (err) {
    console.log(err);
  }
};

export const updatePost = (id, status, postImage) => {
  let docToUpdate = doc(postsRef, id);
  try {
    updateDoc(docToUpdate, { status, postImage });
    toast.success("Postarea a fost actualizată.");
  } catch (err) {
    console.log(err);
  }
};

export const deletePost = (id) => {
  let docToDelete = doc(postsRef, id);
  try {
    deleteDoc(docToDelete);
    toast.success("Postarea a fost ștearsă.");
  } catch (err) {
    console.log(err);
  }
};

export const addConnection = (currentUser, targetId, name) => {
  try {
    let connectionToAdd = doc(connectionRef, `${currentUser.id}_${targetId}`);
    let chatUsersAdd = doc(chatUsersRef, currentUser.id);
    const chatRoomId = getUniqueID();
    setDoc(connectionToAdd, {
      userId: currentUser.id,
      targetId,
      [currentUser.id]: currentUser.name,
      [targetId]: name,
      chatRoomId,
    });
    setDoc(doc(firestore, "chatRooms", chatRoomId), { messages: [] });
    setDoc(chatUsersAdd, { userId: currentUser.id });
    toast.success("Conexiune adăugată.");
  } catch (err) {
    console.log(err);
  }
};

export const getMyConnections = async (userId, setConnections) => {
  try {
    const allConnectionsQuery = query(
      connectionRef,
      or(where("userId", "==", userId), where("targetId", "==", userId))
    );

    const querySnapshot = await getDocs(allConnectionsQuery);
    const connections = [];
    querySnapshot.forEach((doc) => connections.push(doc.data()));
    setConnections(connections);
  } catch (err) {
    console.log(err);
  }
};

export const getConnections = (userId, targetId, setIsConnected) => {
  try {
    let connectionsQuery = query(
      connectionRef,
      where("targetId", "==", targetId)
    );

    onSnapshot(connectionsQuery, (response) => {
      let connections = response.docs.map((doc) => doc.data());

      const isConnected = connections.some(
        (connection) => connection.userId === userId
      );

      setIsConnected(isConnected);
    });
  } catch (err) {
    console.log(err);
  }
};

export const postChatMessage = (
  message,
  timeStamp,
  currentUser,
  connection
) => {
  try {
    const chatRoomDocRef = doc(chatRoomsRef, connection.chatRoomId);
    updateDoc(chatRoomDocRef, {
      messages: arrayUnion({ message, timeStamp, name: currentUser.name }),
    });
  } catch (err) {
    console.log(message);
    console.log(err);
  }
};

export const getChatMessages = (connection, setMessages) => {
  try {
    const chatRoomDocRef = doc(chatRoomsRef, connection.chatRoomId);
    onSnapshot(chatRoomDocRef, (response) => {
      const data = response.data();
      setMessages(data.messages.reverse());
    });
  } catch (err) {
    console.log(err);
  }
};
