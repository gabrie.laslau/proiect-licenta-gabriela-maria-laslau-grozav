// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from 'firebase/firestore';
import { getStorage } from 'firebase/storage';

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCMEvQGT0XivXkVuyRdbxDVc5_xyYwDboo",
  authDomain: "fitclub--vp.firebaseapp.com",
  projectId: "fitclub--vp",
  storageBucket: "fitclub--vp.appspot.com",
  messagingSenderId: "1099044636251",
  appId: "1:1099044636251:web:a7eb987eeb15bcaab2217e"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const firestore = getFirestore(app);
const storage = getStorage(app);

export { app, auth, firestore ,storage}
