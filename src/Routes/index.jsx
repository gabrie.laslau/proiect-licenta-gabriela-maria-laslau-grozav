import React, { useContext } from 'react'
import { createBrowserRouter } from 'react-router-dom'
import Login from '../Pages/Login'
import Register from '../Pages/Register'
import HomeLayout from '../layouts/HomeLayout'
import ProfileLayout from '../layouts/ProfileLayout'
import ConnectionLayout from '../layouts/ConnectionLayout'
import FitnessLayout from '../layouts/FitnessLayout'
import ChatLayout from '../layouts/ChatLayout'
import MessengerLayout from '../layouts/MessengerLayout'


export const router = createBrowserRouter([
    {
        path: "/",
        element: <Login />,
    },
    {
        path: "/register",
        element: <Register />,
    },
    {
        path: "/home",
        element: <HomeLayout />,
    },
    {
        path: "/profile",
        element: <ProfileLayout />,
    },
    {
        path: "/connections",
        element: <ConnectionLayout />,
    },
    {
        path: "/fitness",
        element: <FitnessLayout />,
    },
    {
        path: "/chat",
        element: <ChatLayout />,
    },
    {
        path: "/Messenger",
        element: <MessengerLayout />,
    },
]);