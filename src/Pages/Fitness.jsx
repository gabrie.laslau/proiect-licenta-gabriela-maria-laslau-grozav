import React, { useEffect, useState } from 'react'
import FitnessComponent from '../components/FitnessComponent'
import { onAuthStateChanged } from 'firebase/auth'
import { useNavigate } from 'react-router-dom'
import { auth } from '../firebaseConfig'
import Loader from '../components/common/Loader'

export default function Fitness({ currentUser }) {
    const [loading, setLoading] = useState(true)
    let navigate = useNavigate()
    useEffect(() => {
        onAuthStateChanged(auth, (res) => {
            if (!res?.accessToken) {
                navigate('/fitness')
            }
            else {
                setLoading(false)
            }
        })
    }, [])
    return loading ? <Loader /> : <FitnessComponent currentUser={currentUser} />
}