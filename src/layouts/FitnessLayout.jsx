import React, { useMemo, useState } from 'react'
import Topbar from '../components/common/Topbar'
import { getCurrentUser } from '../api/FirestoreAPI'
import Fitness from '../Pages/Fitness'

export default function FitnessLayout() {
    const [currentUser, setCurrentUser] = useState({})
    useMemo(() => {
        getCurrentUser(setCurrentUser)
    }, [])
    return (
        <div>
            <Topbar currentUser={currentUser} />
            <Fitness currentUser={currentUser}/>
        </div>
    )
}
