import React, { useMemo, useState } from 'react'
import MessengerPage from '../Pages/MessengerPage'
import Topbar from '../components/common/Topbar'
import { getCurrentUser } from '../api/FirestoreAPI'

export default function MessengerLayout() {
    const [currentUser, setCurrentUser] = useState({})
    useMemo(() => {
        getCurrentUser(setCurrentUser)
    }, [])
    return (
        <div>
            <Topbar currentUser={currentUser} />
            <MessengerPage currentUser={currentUser} />
        </div>
    )
}