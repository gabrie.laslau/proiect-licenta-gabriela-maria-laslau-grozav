import { useMemo, useState } from "react";
import ChatPage from "../Pages/ChatPage";
import Topbar from "../components/common/Topbar";
import { getCurrentUser } from "../api/FirestoreAPI";

export default function ChatLayout() {
  const [currentUser, setCurrentUser] = useState({});
  useMemo(() => {
    getCurrentUser(setCurrentUser);
  }, []);
  return (
    <div>
      <Topbar currentUser={currentUser} />
      <ChatPage currentUser={currentUser} />
    </div>
  );
}
